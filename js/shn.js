var BASE_URL = "http://yourdomain";

$(document).ready(function(){
	$('#url_form').submit(function (e) {
		shn.checkUrlInput();
	});

	$("#custom_short_url").keyup(shn.throttle(function(){
		shn.checkShortURL();
	}));
});

var shn = {
	createCustomCode: function(){
		if(confirm("Do you want to enter custom short URL? (e.g. YesIWant for "+BASE_URL+"/r/YesIWant) If not, an auto generated short URL will be given to you...")){
			$("#custom_short_cont").show();
		}
	},
	checkUrlInput: function(){
		var url = $("#url_input").val();
		if(url == ''){
			alert("We can't sho'n 'nothing'!");
			$("#url_input").val("");
			return false;
		}else{
			if(!shn.isUrlValid(url)){
				alert("Please enter a valid URL!");
				$("#url_input").val("");
				return false;
			}
		}
	},
	checkShortURL: function(){
		var shortCode = $("#custom_short_url").val().replace(" ", ""); //remove spaces

		if(shortCode != ''){
			if(shortCode.length > 2){
				$.ajax({
					type: 'POST',
					url: BASE_URL+'/',
					data: {
						short_code: shortCode
					}
				}).done(function(response){
					if(response == false){
						$("#validation_text").css("color", "green").text(" is available.");
					}else{
						$("#validation_text").css("color", "red").text(" already exists.");
					}
					return;

				}).fail(function(response){
					shn.checkShortURL();
				});
			}
		}
	},
	checkMinChars: function(){
		var codeLen = $("#custom_short_url").val().replace(" ", "").length;
		if(codeLen < 3){
			if(codeLen == 0){
				$("#custom_short_cont").hide();
			}else{
				$("#validation_text").css("color", "red").text(" minimum of 3 characters required.");
			}
		}
	},
	throttle: function(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
            f.apply(context, args);
        },
        delay || 500);
    };
	},
	isUrlValid: function(url){
		return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}
}

