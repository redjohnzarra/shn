<?php
	require_once dirname(__FILE__).'/shn.php';

	$shnObj = new Shn();

	$app->post("/shn", function() use($app, $shnObj){
		$url = $app->request->post('url');
		$shortCode = $app->request->post('short_code');
		if(!empty($url)){
			$shn = $shnObj->getShn($url, $shortCode);
			$app->render('shorten.twig', array("shn" => $shn));
		}else{
			echo "Please type a url...";
		}
	});

	$app->get('/:code', function ($name) use($app, $shnObj) {
	    $url = $shnObj->getUrlFromCode($name);
	    if($url == false){
	  //   	$app->notFound(function () use ($app) {
			//     $app->render('404.html');
			// });
			$app->notFound();
	    }else{
	    	$app->response->redirect($url);
	    }
	});


?>
