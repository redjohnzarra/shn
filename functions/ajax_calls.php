<?php
	// AJAX CALLS
	$app->post('/', function() use($app, $shnObj){
		$shortCode = $app->request->post('short_code');
		if(!empty($shortCode)){
			$res = $shnObj->checkIfCodeExists($shortCode);
			echo $res;
		}else{
			echo false;
		}
	});
