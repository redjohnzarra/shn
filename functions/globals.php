<?php
	require_once dirname(__FILE__).'/config.php';

	function connectToDb(){
		$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD) or die("Unable to connect...");
		mysqli_select_db($con, DB_NAME) or die("Could not open the db");

		return $con;
	}

	function disconnectDb($con){
		$con->close();
	}

	function getShnComplete($code){
		return SHN_PREFIX.$code;
	}
