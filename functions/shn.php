<?php
	require_once dirname(__FILE__).'/globals.php';

/**
 * This class contains all the functions needed for Sho'n.
 */
class Shn
{
	/**
	 * contains the connection to the database.
	 * @var obj
	 */
	protected $con;

	protected static $tableName = "sho_en";

	protected static $charset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	protected static $codeLengthDefault = 3;

	public function __construct(){
		$this->con = connectToDb();
		$this->tb = self::$tableName;
	}

	public function checkUrlExists($url){
		$sql = "SELECT * FROM $this->tb WHERE url = '$url'";
		$result = $this->con->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				return $row['code'];
			}
		}else{
			return false;
		}
	}

	public function getShn($url, $shortCode){
		$code = $this->checkUrlExists($url);
		if($code == false){
			$code = $shortCode == "" ? $this->getCode() : $shortCode;
			$this->insertUrlIntoDb($url, $code);
		}

		return getShnComplete($code);
	}

	public function insertUrlIntoDb($url, $code){
		$sql = "INSERT INTO $this->tb (url, code, date_time) VALUES ('$url', '$code', NOW())";
		$this->con->query($sql);
	}

	public function getCode(){
		$codeExists = true;
		while($codeExists){
			$code = substr(str_shuffle(str_repeat(self::$charset, self::$codeLengthDefault)), 0, self::$codeLengthDefault);
			$codeExists = $this->checkIfCodeExists($code);
		}

		return $code;
	}

	public function checkIfCodeExists($code){
		$sql = "SELECT * FROM $this->tb WHERE code = '$code'";
		$result = $this->con->query($sql);
		if($result->num_rows > 0){
			return true;
		}else{
			return false;
		}
	}

	public function getUrlFromCode($code){
		$sql = "SELECT url FROM $this->tb WHERE code = '$code'";
		$result = $this->con->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				return $row['url'];
			}
		}else{
			return false;
		}
	}
}

?>
